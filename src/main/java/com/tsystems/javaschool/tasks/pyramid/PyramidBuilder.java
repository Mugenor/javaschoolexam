package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null) throw new CannotBuildPyramidException();
        double sqrtDiscriminant = Math.sqrt(1.0d + 8.0d * inputNumbers.size());
        if (sqrtDiscriminant % 1 != 0) {
            throw new CannotBuildPyramidException();
        }
        int height = ((int) sqrtDiscriminant) / 2;
        int width = 2 * height - 1;

        int[][] pyramid = new int[height][width];

        try {
            inputNumbers.sort(Comparator.naturalOrder());
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }

        Iterator<Integer> iterator = inputNumbers.iterator();

        int currOffset, countOnCurrRow;
        for (int i = 0; i < pyramid.length; ++i) {
            currOffset = pyramid[i].length / 2 - i;
            countOnCurrRow = i + 1;
            for (int j = 0; j < pyramid[i].length; ++j) {
                if (j >= currOffset && (j - currOffset) % 2 == 0 && countOnCurrRow > 0) {
                    pyramid[i][j] = iterator.next();
                    --countOnCurrRow;
                } else {
                    pyramid[i][j] = 0;
                }
            }
        }

        return pyramid;
    }


}
