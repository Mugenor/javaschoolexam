package com.tsystems.javaschool.tasks.calculator;


import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayDeque;
import java.util.Deque;

public class Calculator {
    private static final DecimalFormat df;

    static {
        DecimalFormatSymbols symbol = new DecimalFormatSymbols();
        symbol.setDecimalSeparator('.');
        df = new DecimalFormat("#.####", symbol);
        df.setRoundingMode(RoundingMode.CEILING);
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement == null) return null;
        String polishNotation;
        try {
            polishNotation = toPolishNotation(statement);
            return df.format(calculateFromPolishNotation(polishNotation));
        } catch (RuntimeException e) {
            return null;
        }
    }

    private double calculateFromPolishNotation(String polishNotation) {
        Deque<Double> stack = new ArrayDeque<>();
        char ch;
        double a;
        String[] values = polishNotation.split(" +");
        for (int i = 0; i < values.length; ++i) {
            try {
                stack.push(Double.parseDouble(values[i]));
            } catch (NumberFormatException e) {
                ch = values[i].charAt(0);
                switch (ch) {
                    case '+':
                        stack.push(stack.pop() + stack.pop());
                        break;
                    case '-':
                        stack.push(- stack.pop() + stack.pop());
                        break;
                    case '*':
                        stack.push(stack.pop() * stack.pop());
                        break;
                    case '/':
                        a = stack.pop();
                        if(a == 0.0d) throw new RuntimeException();
                        stack.push(stack.pop() / a);
                        break;
                }
            }
        }

        return stack.pop();
    }

    private String toPolishNotation(String statement) {
        StringBuilder operationStack = new StringBuilder();
        StringBuilder polishNotation = new StringBuilder();
        char prevChar = 0, currChar, tmpChar;

        for (int i = 0; i < statement.length(); ++i) {
            currChar = statement.charAt(i);
            if (isOperation(currChar)) {

                if (isOperation(prevChar) || prevChar == '(' || prevChar == '.')
                    throw new RuntimeException("Bad expression");

                while (operationStack.length() > 0) {
                    tmpChar = operationStack.charAt(operationStack.length() - 1);
                    if (isOperation(tmpChar) && (getPriorityOfOperation(currChar) <= getPriorityOfOperation(tmpChar))) {
                        polishNotation.append(" ").append(tmpChar);
                        operationStack.setLength(operationStack.length() - 1);
                    } else {
                        break;
                    }
                }
                operationStack.append(currChar);
                polishNotation.append(" ");
            } else if (currChar == '(') {
                operationStack.append(currChar);
            } else if (currChar == ')') {
                tmpChar = operationStack.charAt(operationStack.length() - 1);
                while (tmpChar != '(') {
                    polishNotation.append(" ").append(tmpChar);

                    operationStack.setLength(operationStack.length() - 1);
                    tmpChar = operationStack.charAt(operationStack.length() - 1);
                }
                operationStack.setLength(operationStack.length() - 1);
                polishNotation.append(" ");
            } else if ((currChar >= '0' && currChar <= '9') || currChar == '.') {
                if (prevChar == ')' || (currChar == '.' && isOperation(prevChar)))
                    throw new RuntimeException("Bad expression");
                polishNotation.append(currChar);
            } else {
                throw new RuntimeException("Bad symbols");
            }
            prevChar = currChar;
        }

        while (operationStack.length() > 0) {
            tmpChar = operationStack.charAt(operationStack.length() - 1);
            if (tmpChar == '(' || tmpChar == ')') throw new RuntimeException("Bad expression");
            polishNotation.append(" ").append(tmpChar);
            operationStack.setLength(operationStack.length() - 1);
        }
        return polishNotation.toString();
    }


    private boolean isOperation(char ch) {
        switch (ch) {
            case '+':
            case '-':
            case '*':
            case '/':
                return true;
        }
        return false;
    }

    private int getPriorityOfOperation(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 3;
        }
        return -1;
    }

}
